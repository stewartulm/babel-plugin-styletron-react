
export default function plugin({ types: t }) {
  return {
    visitor: {
      VariableDeclaration(declarationPath, state){
        declarationPath.traverse({
          VariableDeclarator(variablePath) {
            if (t.isCallExpression(variablePath.node.init)) {
              let methodName = variablePath.node.init.callee.name
              let displayName = variablePath.node.id.name
              let lib = state.file.metadata.modules.imports.find(i => i.source === 'styletron-react')
              if (lib && lib.specifiers.find(s => s.imported === 'styled' && s.local === methodName)) {
                declarationPath.insertAfter(t.expressionStatement(
                  t.assignmentExpression(
                    '=',
                    t.memberExpression(t.identifier(displayName), t.identifier('displayName')),
                    t.stringLiteral(displayName))))
              }
            }
          }
        })
      }
    }
  }
}
