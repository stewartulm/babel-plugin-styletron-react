# `babel-plugin-styletron-react`

Babel plugin for `styletron-react`. (Shameless lifted from `babel-plugin-styled-components`.)

## Usage

```
yarn add --dev babel-plugin-styletron-react
```

Then in your babel configuration (probably `.babelrc`):

```JSON
{
  "plugins": [ "styletron-react" ]
}
```

## Features

### Add `displayNames` to your components

By showing your components' real name in the React DevTools it's much easier to debug your applications.

This plugin takes the name of the variable you assign your `styled` styletron component to, and adds it as the `displayName` to the resulting React component.

```JS
const MyBtn = styled('button', { })
// Plugin does this for you:
MyBtn.displayName = 'MyBtn'
```

When rendering this button, the React DevTools will normally just show `<StyledElement>`. By enabling this plugin, the DevTools show `<MyBtn />`.

## License

Licensed under the MIT License, Copyright © 2017 Stewart Ulm

See [LICENSE.md](./LICENSE.md) for more information.
